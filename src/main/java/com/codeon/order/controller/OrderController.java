package com.codeon.order.controller;

import com.codeon.order.common.OrderRequest;
import com.codeon.order.common.OrderResponse;
import com.codeon.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService service;

    @GetMapping("/")
    public String welcome() {
        return "Order Service Active!";
    }

    @PostMapping("/bookOrder")
    public OrderResponse createOrder(@RequestBody OrderRequest request) {
        return service.saveOrder(request);
    }
}
