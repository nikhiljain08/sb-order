package com.codeon.order.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payment {

    private String _id;
    private String paymentStatus;
    private String transactionId;
    private double amount;
}
