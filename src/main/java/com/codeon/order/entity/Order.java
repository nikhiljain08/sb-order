package com.codeon.order.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ORDER_TB")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    @Id
    private String _id;
    private double offerAmount;
    private double deliveryFee;
    private String payMode;
    private double totalAmount;

    @Getter(AccessLevel.NONE)
    private String userId;

    @Getter(AccessLevel.NONE)
    private String addressId;
}
