package com.codeon.order.service;

import com.codeon.order.common.OrderRequest;
import com.codeon.order.common.OrderResponse;
import com.codeon.order.common.Payment;
import com.codeon.order.common.Product;
import com.codeon.order.entity.Order;
import com.codeon.order.entity.OrderProduct;
import com.codeon.order.repository.OrderRepository;
import com.codeon.order.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    private static final String PAYMENT_ENDPOINT_URL = "/payment/doPayment";
    private static final String PRODUCT_ENDPOINT_URL = "/product/";

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RestTemplate template;

    @Autowired
    private DiscoveryClient client;
    
    public OrderResponse saveOrder(OrderRequest request) {
        Order order = request.getOrder();
        Payment payment = new Payment();
        Order saved_order = orderRepository.save(order);
        payment.setAmount(saved_order.getTotalAmount());

        String pay_url = getInstanceURL(client.getInstances("PAYMENT-SERVICE").get(0));
        Payment paymentResponse = template.postForObject(pay_url + PAYMENT_ENDPOINT_URL, payment, Payment.class);
        String response = paymentResponse.getPaymentStatus().equals("success") ?
                "payment processing successful and order placed" : "there is a failure in payment api , order added to cart";

        List<Product> products = new ArrayList<>();
        for(OrderProduct orderProduct : request.getOrderProducts()) {
            String prod_url = getInstanceURL(client.getInstances("PRODUCT-SERVICE").get(0));
            Product productResponse = template.getForObject(prod_url + PRODUCT_ENDPOINT_URL + orderProduct.getProductId(), Product.class);
            Product product = new Product();
            orderProduct = productRepository.save(orderProduct);
            product.set_id(orderProduct.get_id());
            product.setQty(orderProduct.getQty());
            product.setPrice(orderProduct.getPrice());
            product.setQty_type(productResponse.getQty_type());
            product.setDescription(productResponse.getDescription());
            product.setName(productResponse.getName());
            product.setImg_url(productResponse.getImg_url());
            products.add(product);
        }

        return new OrderResponse(order, products, paymentResponse.getAmount(), paymentResponse.getTransactionId(), response);
    }

    private String getInstanceURL(ServiceInstance payment_serviceInstance) {
        if(!payment_serviceInstance.getHost().contains("localhost")) {
            URI uri;
            try {
                uri = new URI("https", payment_serviceInstance.getUri().getHost(),
                        payment_serviceInstance.getUri().getPath(), payment_serviceInstance.getUri().getFragment());
                return uri.toString();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return payment_serviceInstance.getUri().toString();
    }
}
