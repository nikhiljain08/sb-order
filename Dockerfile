FROM openjdk:11
ARG JAR_FILE=build/libs/order-0.0.1.jar
COPY ${JAR_FILE} order.jar
ENTRYPOINT ["java","-jar","/order.jar"]